﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" Theme="light" %>

    <asp:Content runat="server" ContentPlaceHolderID="title">
       <h3>SQL DATA BASE</h3> 
        </asp:Content>
    <asp:Content runat="server" ContentPlaceHolderID="concept">
    <p> Structured Query Language (SQL) is a specialized language for updating, deleting, and requesting information from databases.
        SQL is an ANSI and ISO standard, and is the de facto standard database query language.</p>
        </asp:Content>
    <asp:Content runat="server" ContentPlaceHolderID="links">
       <p>LINKS</p> 
    <a href="https://www.w3schools.com/sql/default.asp"> SQL Tutorial </a><br>
        </asp:Content>          
<asp:Content runat="server" ContentPlaceHolderID="dataGrid">
        <asp:CodeBox ID="sql" runat="server" SkinId="CodeBox" code="sql_code" owner="Me" codeType="sql">
         </asp:CodeBox>
     </asp:Content>