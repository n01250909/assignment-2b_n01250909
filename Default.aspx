﻿<%@ Page Language="C#" Inherits="Assignment_2a_N01250909.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Study Guide</title>
    <link href="/Content/bootstrap.min.css" rel="stylesheet">
</head>
    <!--style for default page-->
       <style>
            nav ul{
                list-style-type:none;
            }
            nav ul li{
                display:inline-block;
                margin-top: 50px;
                padding: 20px;
                text-transform: uppercase;
       
            }
            header{ 
                position:relative;
                text-align: center;
            }
            nav {
                position:absolute;
                top:10px;
                right:700px;
            }
        </style>
<body>
    <header>
             <h1>Welcome to Web Development Study Guide</h1>
        <nav id="nav">
           <ul class="menu">
                <li><a class="menu-list" href="/Javascript.aspx">Javascript</a></li> 
                <li><a class="menu-list" href="/Html.aspx">Html</a></li> 
                <li><a class="menu-list" href="/Sql.aspx">Sql</a></li>    
            </ul>
        </nav>      
    </header>
</body>
</html>