﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment_2a_N01250909
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string CodeType
        {
            get { return (string)ViewState["CodeType"]; }
            set { ViewState["CodeType"] = value; }
        }
        DataView CreateCodeSource()
        {
            DataTable tabledata = new DataTable();
            DataColumn code_col = new DataColumn();
            DataRow coderow;
            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            tabledata.Columns.Add(code_col);
            List<string> sql_code = new List<string>(new string[]{/*this is example code that will show on a page*/
                "SELECT studentfname, studentlname, studentid",
                "FROM students",
                "WHERE studentfname like '%Maria%' and studentlname like '%Korolenko%'",
                "OR studentid like 'n01250909';"});
            List<string> js_code = new List<string>(new string[]{
                "var student = {",
                "~~firstName: ^Maria^,",
                "~~lastName: ^Korolenko^,",
                "~~studentId: ^n01250909^,",
                "~~section: ^A^",
                "}"});
            List<string> html_code = new List<string>(new string[]{
                "<!DOCTYPE html>",
                "<html>",
                "<body>",
                "",
                "<h1>My First Heading</h1>",
                "<p>My first paragraph.</p>",
                "",
                "</body>",
                "</html>",
                "",
                "",
                "body {",
                "~~font-family: Arial, sans-serif;",
                "}",
                "h1 {",
                "~~color: blue;",
                "}"});
            List<string> showCode = new List<string>();
            if (CodeType == "sql" ){
                showCode = sql_code;
            }else if (CodeType == "js"){
                showCode = js_code;
            }else if (CodeType == "html"){
                showCode = html_code;
            }
            foreach (string code_line in showCode)
            {
                coderow = tabledata.NewRow();
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                formatted_code = formatted_code.Replace("^", "&quot;");
                coderow[code_col.ColumnName] = formatted_code;
                tabledata.Rows.Add(coderow);
            }
            DataView codeview = new DataView(tabledata);
            return codeview;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            DataView dv = CreateCodeSource();
            Code.DataSource = dv;
            Code.DataBind();
        }

    }

}
