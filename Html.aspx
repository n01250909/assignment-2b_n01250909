﻿ <%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" Theme="light" %>

<asp:Content runat="server" ContentPlaceHolderID="title">
       <h3>HTML / CSS</h3> 
        </asp:Content>
    <asp:Content runat="server" ContentPlaceHolderID="concept">
        HTML consists of a series of short codes typed into a text-file by the site author - these are the tags. 
        The text is then saved as a html file, and viewed through a browser, like Internet Explorer or Netscape Navigator. 
        This browser reads the file and translates the text into a visible form, hopefully rendering the page as the author had intended. 
        Writing your own HTML entails using tags correctly to create your vision. You can use anything from a rudimentary text-editor to a powerful graphical editor to create HTML pages.
        </asp:Content>
    <asp:Content runat="server" ContentPlaceHolderID="links">
       <p><br>LINKS</p> 
    <a href="https://www.w3schools.com/html/default.asp"> HTML </a><br>
        </asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="dataGrid">
        <asp:CodeBox ID="html" runat="server" SkinId="CodeBox" code="html_code" owner="Me" codeType="html">
         </asp:CodeBox>
     </asp:Content>